---
title: "Identify the overlap between FIMO sites matching the ANT in vitro binding site and ANT ChIP-Seq peaks"
author: "Beth Krizek"
date: "8/18/2019"
output: html_document
---

## Introduction

Previously, Ivory used FIMO from The MEME Suite to identify sequences in the Arabidopsis genome with similarity to the ANT in vitro determined consensus sequence (JASPAR MA0571.1). Here we investigate the overlap of these FIMO sites with the set of ANT peaks identified in the stage 6/7 ChIP-Seq experiment. 

* We ask the following questions:
* 1. What percentage of ANT peaks contain at least one FIMO site?
* 2. Are the occurance of FIMO sites in ANT peaks enriched compared to their occurance in randomized peak regions?

##Methods

* The starting files are the output from FIMO: ANT3fimo.gff (sites with p value <-3) and ANT4fimo.gff (sites with p value <-4). Modify these files to remove FIMO sites in chrC and chrM and sort by position on chromosomes 1-5. 
```{bash}
grep 'chr1' ./data/ANT3fimo.gff | sort -k4,4 -n > ./results/ANT3fimochr1sort.gff
grep 'chr2' ./data/ANT3fimo.gff | sort -k4,4 -n > ./results/ANT3fimochr2sort.gff
grep 'chr3' ./data/ANT3fimo.gff | sort -k4,4 -n > ./results/ANT3fimochr3sort.gff
grep 'chr4' ./data/ANT3fimo.gff | sort -k4,4 -n > ./results/ANT3fimochr4sort.gff
grep 'chr5' ./data/ANT3fimo.gff | sort -k4,4 -n > ./results/ANT3fimochr5sort.gff
cat ./results/ANT3fimochr1sort.gff ./results/ANT3fimochr2sort.gff ./results/ANT3fimochr3sort.gff ./results/ANT3fimochr4sort.gff ./results/ANT3fimochr5sort.gff > ./results/ANT3fimoallchrsort.gff
grep 'chr1' ./data/ANT4fimo.gff | sort -k4,4 -n > ./results/ANT4fimochr1sort.gff
grep 'chr2' ./data/ANT4fimo.gff | sort -k4,4 -n > ./results/ANT4fimochr2sort.gff
grep 'chr3' ./data/ANT4fimo.gff | sort -k4,4 -n > ./results/ANT4fimochr3sort.gff
grep 'chr4' ./data/ANT4fimo.gff | sort -k4,4 -n > ./results/ANT4fimochr4sort.gff
grep 'chr5' ./data/ANT4fimo.gff | sort -k4,4 -n > ./results/ANT4fimochr5sort.gff
cat ./results/ANT4fimochr1sort.gff ./results/ANT4fimochr2sort.gff ./results/ANT4fimochr3sort.gff ./results/ANT4fimochr4sort.gff ./results/ANT4fimochr5sort.gff > ./results/ANT4fimoallchrsort.gff
wc -l ./results/ANT3fimoallchrsort.gff
wc -l ./results/ANT4fimoallchrsort.gff
```

* Convert FIMO gff files to bed files
```{bash}
convert2bed -i gff < ./results/ANT3fimoallchrsort.gff > ./results/ANT3fimo.bed
convert2bed -i gff < ./results/ANT4fimoallchrsort.gff > ./results/ANT4fimo.bed
```

* Replace Chr1 with chr1 in ANT peaks files
```{bash}
sed 's/C/c/' ../FindClosestGene/data/chr1.1000.bed > ./results/revchr1.1000.bed
sed 's/C/c/' ../FindClosestGene/data/chr2.1000.bed > ./results/revchr2.1000.bed
sed 's/C/c/' ../FindClosestGene/data/chr3.1000.bed > ./results/revchr3.1000.bed
sed 's/C/c/' ../FindClosestGene/data/chr4.1000.bed > ./results/revchr4.1000.bed
sed 's/C/c/' ../FindClosestGene/data/chr5.1000.bed > ./results/revchr5.1000.bed
```

* Make a single file with all ANT peaks
```{bash}
cat ./results/revchr1.1000.bed ./results/revchr2.1000.bed ./results/revchr3.1000.bed ./results/revchr4.1000.bed ./results/revchr5.1000.bed > ./results/revallchr.1000.bed
head ./results/revallchr.1000.bed
wc -l ./results/revallchr.1000.bed
```

* Identify and count FIMO sites that completely overlap an ANT peak
```{bash}
bedtools intersect -a ./results/revallchr.1000.bed -b ./results/ANT3fimo.bed -F 1.00 > ./results/fimo3chipoverlap.bed
bedtools intersect -a ./results/revallchr.1000.bed -b ./results/ANT4fimo.bed -F 1.00 > ./results/fimo4chipoverlap.bed
wc -l ./results/fimo3chipoverlap.bed
wc -l ./results/fimo4chipoverlap.bed
```
 
* Note that some ANT peaks are associated with more than one FIMO site. This explains the 1158 FIMO sites that match 1113 ANT peaks. 

* Identify and count ANT peaks that do not contain a FIMO site
```{bash}
bedtools intersect -v -a ./results/revallchr.1000.bed -b ./results/ANT3fimo.bed > ./results/nooverlapfimo3.bed
bedtools intersect -v -a ./results/revallchr.1000.bed -b ./results/ANT4fimo.bed > ./results/nooverlapfimo4.bed
wc -l ./results/nooverlapfimo3.bed
wc -l ./results/nooverlapfimo4.bed
```

* Generate a tab delimited file with two columns indicating the length of each Arabidopsis chromosome for use in bedtools shuffle
* I used chromosome lengths from IGB
```{r}
Arabgenome <- data.frame ("chr"= c("chr1", "chr2", "chr3", "chr4", "chr5"), "length" = c(30427671, 19698289, 23459830, 18585056, 26975502))
head(Arabgenome)
write.table(Arabgenome, './results/Arabgenome.txt', sep="\t", col.names=F, row.names = F, quote = F)
```

* Use bedtools shuffle to generate a set of randomized peaks for testing statistical significance

* Bedtools shuffle repositions each ANT peak on a random chromosome at a random position. Because bedtools shuffle uses a pseudo-random number generator to permute the locations, each run should produce a different result. This can be changed by supplying a custom integer seed (-seed number).
```{bash}
bedtools shuffle -i ./results/revallchr.1000.bed -g ./results/Arabgenome.txt > ./results/shuffledrevallchr.1000.bed
head ./results/shuffledrevallchr.1000.bed
wc -l ./results/shuffledrevallchr.1000.bed
```

* Identify and count FIMO sites that completely overlap a randomized peak
```{bash}
bedtools intersect -a ./results/shuffledrevallchr.1000.bed -b ./results/ANT3fimo.bed -F 1.00 > ./results/fimo3shuffledoverlap.bed
bedtools intersect -a ./results/shuffledrevallchr.1000.bed -b ./results/ANT4fimo.bed -F 1.00 > ./results/fimo4shuffledoverlap.bed
wc -l ./results/fimo3shuffledoverlap.bed
wc -l ./results/fimo4shuffledoverlap.bed
```

* Identify and count shuffled peaks that do not contain a FIMO site
```{bash}
bedtools intersect -v -a ./results/shuffledrevallchr.1000.bed -b ./results/ANT3fimo.bed > ./results/nooverlapshuffledfimo3.bed
bedtools intersect -v -a ./results/shuffledrevallchr.1000.bed -b ./results/ANT4fimo.bed > ./results/nooverlapshuffledfimo4.bed
wc -l ./results/nooverlapshuffledfimo3.bed
wc -l ./results/nooverlapshuffledfimo4.bed
```

### Session info
```{r}
sessionInfo()
```

## Results and Conclusions
A FIMO motif is more likely to be present in the collection of ANT peaks than a randomized collections of peaks of the same size. This is true for FIMO sites defined by a p-value of 10-3 or a p-value of 10-4. 66.7% ((1113-370)/1113) of ANT peaks overlap a FIMO 10-3 motif, while 43.8% ((1113-626)/1113) of randomized peaks overlap a FIMO 10-3 motif. 26.4% ((1113-819)/1113) of ANT peaks overlap a FIMO 10-4 motif, while 5.7% ((1113-1050)/1113) of randomized peaks overlap a FIMO 10-4 motif.

Thus, a signficant number of ANT peaks do not contain a sequence with strong similarity to the ANT in vitro consensus binding site as determined by FIMO. This could mean that ANT recognizes somewhat different sequences in vivo. In addition, ANT may bind to DNA as part of a complex with a completely different DNA binding specificity.
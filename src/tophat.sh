#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load bowtie2
module load tophat
G=A_thaliana_Jun_2009
O=tophat
if [ ! -d $O ]; then
    mkdir $O
fi

# S - sample name
# F - file name
# both need to be passed in using qsub -v option
# To run this run using qsub-doIt.sh, do:
#
#  qsub-doIt.sh _R1_001.fastq.gz tophat.sh >jobs.out 2>jobs.err
#

# use fr-firststrand for dUTP library prep protocol
# https://chipster.csc.fi/manual/library-type-summary.html
#
# 5'---------------------->>>3' RNA
#    /2---->   <-----/1
# 
# read 1 is reverse complement of RNA
# read 2 is same sequence as RNA
#

R2=$(echo "$F" | sed "s/R1/R2/")
tophat -p 8 -I 5000 --library-type fr-firststrand -o $O/$S $G $F $R2


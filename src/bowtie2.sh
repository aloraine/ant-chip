#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l mem=96gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load bowtie2

# S,O,G,F1,F2 defined by qsub -v 
bowtie2 -p 8 -x $G -1 $F1 -2 $F2 -S $O/$S.sam


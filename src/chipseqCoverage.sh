#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR

# S,F defined by qsub -v
# S - sample name
# F - file name; should be file.bam

# from https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html
opts="--binSize 1 --extendReads --normalizeUsing RPGC --effectiveGenomeSize 119481543"
bamCoverage $opts -b $F -o $S.chipseq.bw



#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=32gb
#PBS -l walltime=4:00:00
#PBS -N A

# run like this:
# qsub ant-ail6-macs.sh 1>job.out 2>job.err

cd $PBS_O_WORKDIR
G=119667750 # Arabidopsis
M=~/src/MACS/bin/macs2 
Q=0.01

# controls (background)
# A4,chip,no-tag,rep1
# A10,chip,no-tag,rep2

# treatments (chip)
# AL6
# A5,chip,AL6-tag,rep1
# A11,chip,AL6-tag,rep2

# ANT
# A6,chip,ANT-tag,rep1
# A12,chip,ANT-tag,rep2

N=AIL6
CMD="$M callpeak -f BAM -B -g $G -q $Q --keep-dup auto -t A5.bam A11.bam -c A4.bam A10.bam -n $N"
$CMD 
compressMacs2Files.sh

N=ANT
CMD="$M callpeak -f BAM -B -g $G -q $Q --keep-dup auto -t A6.bam A12.bam -c A4.bam A10.bam -n $N"
$CMD 
compressMacs2Files.sh

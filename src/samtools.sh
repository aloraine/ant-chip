#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load samtools

# S,F defined by qsub -v
samtools view -S -b $F > $S.unsorted.bam
samtools sort $S.unsorted.bam -o $S.bam
rm $S.unsorted.bam
samtools index $S.bam

